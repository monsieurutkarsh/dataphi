const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require('morgan');

app.use(morgan('dev'));
//Database connection
var config = require('./config/index');

app.use('/assets', express.static(__dirname + '/public'));

mongoose.Promise = global.Promise;

mongoose.connect(config.getConnectionUrl());

// var patientInfo = require('./app/models/patient');

// console.log(patientInfo.firstname);

var routes = require('./app/routes.js');

app.get('/',function(req,res){
    res.sendFile(__dirname+ '/public/views/index.html');
});

routes(app,bodyParser);

app.listen(3000, function () {
  console.log('app listening on port 3000!');
})