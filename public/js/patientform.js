
app.controller('patientinfoCtrl', function($scope,$http,$state) {


	$scope.submit = function(isValid) {
		$scope.formSubmitted = true;
		if(isValid){
			$http.post('/api/patientInfo/', $scope.formdata)
			.success(
				function(response){
					$scope.formSubmitted = false;
					$scope.formdata = ''; 
					$state.go("patientDetails");
			});	
		}		 	 
	};
});



