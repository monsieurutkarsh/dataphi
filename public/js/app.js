
var app = angular.module('app', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/patientForm');
    
    $stateProvider
        
        .state('patientInfo', {
            url: '/patientForm',
            templateUrl: 'assets/views/patientForm.html',
            controller: 'patientinfoCtrl',     
        })
        
         .state('patientDetails', {
            url: '/patientDetails',
            templateUrl: 'assets/views/patientDetails.html',
            controller: 'patientDetailsCtrl'
        })

}); 
