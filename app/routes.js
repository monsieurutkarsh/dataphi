var patientInfo = require('./models/patient');


module.exports = function(app,bodyParser){

    app.use(bodyParser.json());

    app.use(bodyParser.urlencoded({extended:true}));
    
    app.get('/api/patientInfo',(req,res)=>{
        patientInfo.find(function(err,info){
            if(err) throw err;
            res.json(info);
        });
    });

    app.post('/api/patientInfo/',(req,res)=>{   
         var newpatient = patientInfo({
                firstname : req.body.firstname,
                lastname : req.body.lastname,
                age: req.body.age,
                dob:req.body.dob,
                gender:req.body.gender,
                phone:req.body.phone,
                fti:req.body.fti,
            });
                
            newpatient.save(function(err){
                var response = {
                    status  : 200,
                    success : ' Patient information successfully added to database'
                }
                res.end(JSON.stringify(response));

                if(err) res.end(error);;
            });
    }) 

  }

