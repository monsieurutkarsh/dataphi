var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var patientSchema = new Schema({
    firstname:String,
    lastname:String,
    age:Number,
    dob:Date,
    gender:String,
    phone:Number,
    fti:String,
});

var Patient = mongoose.model('Patient',patientSchema);

module.exports = Patient;