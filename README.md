### **Patient Directory...** ###

MEAN Stack patient directory Single Page Application

Config folder contains mongoDb URL, password and username.

app folder contains files with post, get request to the server and patient model for mongoDb.

Public folder contains all files related to angular and frontend. I have used UI-ROUTER for routing purpose.

Running the application on local computer :-

1- Download/clone the files

2- Install all dependencies using npm install

3- Start the project using command node server.js or nodemon server.js